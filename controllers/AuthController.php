<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\middlewares\AuthMiddleware;
use app\core\middlewares\GuestMiddleware;
use app\models\{UserModel, LoginModel, SubscribeModel};

class AuthController extends Controller {

    public function __construct()
    {
        $this->registerMiddleware(new AuthMiddleware(['logout']));
        $this->registerMiddleware(new AuthMiddleware(['subscribtions']));
        $this->registerMiddleware(new GuestMiddleware(['login']));
        $this->registerMiddleware(new GuestMiddleware(['register']));
    }

    public function login() {
        $model = new LoginModel();
        if ( Application::$app->request->getMethod() === "post" ) {
            
            $model->loadData(Application::$app->request->getBody());

            if ( $model->validate() && $model->login() ) {
                Application::$app->session->setFlash('success', 'Welcome '. Application::$app->user->firstname.'! You are now logged in!');
                Application::$app->response->redirect('/');
            }
        }

        $this->assign('model', $model);

        return $this->render('login');
    }

    public function subscribtions() {
        $subscribtions = SubscribeModel::findMany([]);

        $this->assign('subscribtions', $subscribtions);

        return $this->render('subscribtions');
    }

    public function logout() {
        Application::$app->logout();
        Application::$app->session->setFlash('success', 'Logged out! Come again soon!');
        Application::$app->response->redirect('/');
    }

    public function register() {
        $model = new UserModel();
        if ( Application::$app->request->getMethod() === "post" ) {
            
            $model->loadData(Application::$app->request->getBody());

            if ( $model->validate() && $model->save() ) {
                Application::$app->session->setFlash('success', 'Thanks for registering <b>'.$model->firstname.'</b>! You can now login with your email address <b>'.$model->email.'</b> and the password you created.');
                Application::$app->response->redirect('/');
            }
        }

        $this->assign('model', $model);

        return $this->render('register');
    }
}