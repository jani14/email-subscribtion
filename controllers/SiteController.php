<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\models\SubscribeModel;

class SiteController extends Controller {



    public function home() 
    {
        $subscribeModel = new SubscribeModel();

        $subscribed = Application::$app->session->getFlash('subscribed') == 'success' ? true : false;
        $this->assign('subscribed', $subscribed);
        $this->assign('subscribeModel', $subscribeModel);
        return $this->render('home');
    }

    public function subscribe() 
    {

        $subscribeModel = new SubscribeModel();
        $subscribeModel->loadData(Application::$app->request->getBody());

        if ( $subscribeModel->validate() && $subscribeModel->save() ) {
            Application::$app->session->setFlash('subscribed','success');
        } else {
            Application::$app->session->setFlash('subscribed','false');
            Application::$app->session->setFlash('subscribe_error',$subscribeModel->getFirstError('email'));
        }

        Application::$app->response->redirect('/');
    }    
}