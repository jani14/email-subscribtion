<?php

namespace app\core;

class Session {

    protected const FLASH_KEY = 'flash_messages';

    public function __construct()
    {  
       session_start(); 
       $flashMessages = $_SESSION[self::FLASH_KEY] ?? [];

       foreach ( $flashMessages as $key => &$flashMessage ) {
            $flashMessage['remove'] = true;
       }

       $_SESSION[self::FLASH_KEY] = $flashMessages;
    }

    public function set(string $key, string $message) {
        $_SESSION[$key] = ['remove' => false, 'value' => $message];
    }

    public function get(string $key) {
        return $_SESSION[$key]['value'] ?? false;
    }

    public function setFlash(string $key, string $message) {
        $_SESSION[self::FLASH_KEY][$key] = ['remove' => false, 'value' => $message];
    }

    public function getFlash(string $key) {
        return $_SESSION[self::FLASH_KEY][$key]['value'] ?? false;
    }

    public function remove(string $key) {
        unset($_SESSION[$key]);
    }


    public function __destruct()
    {
        $flashMessages = $_SESSION[self::FLASH_KEY] ?? [];

        foreach ( $flashMessages as $key => $flashMessage ) {
            $remove = $flashMessage['remove'] ?? true;
            if ( $remove ) {
                unset($flashMessages[$key]);
            }
        }
 
        $_SESSION[self::FLASH_KEY] = $flashMessages;
    }
}