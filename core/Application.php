<?php

namespace app\core;


/**
 * Application core class
 * 
 * @author Jani <jani.paakkola@hotmail.com>
 * @package app\core
 */
class Application {


    public string $userClass;

    /**
     * Web server root directory
     *
     * @var string
     */
    public static string $ROOT_DIR;

    /**
     * Static instance of the app to access from other parts
     *
     * @var Application
     */
    public static Application $app;


    /**
     * Router to handle routing
     *
     * @var Router
     */
    public Router $router;

    /**
     * Request to handle request
     *
     * @var Request
     */
    public Request $request;

    /**
     * Response class
     *
     * @var Response
     */
    public Response $response;


    public Session $session;


    public ?Database $db = null;

    protected Controller $controller;


    public ?DbModel $user;

    /**
     * Construct
     *
     * @param string $rootDir
     */
    public function __construct(string $rootDir, array $config = [])
    {
        self::$ROOT_DIR = $rootDir;
        
        $this->request = new Request();
        $this->response = new Response();
        $this->router = new Router($this->request, $this->response);
        $this->db = new Database($config['db']);
        $this->session = new Session();
        $this->userClass = $config['userClass'];
        $this->user = null;
        self::$app = $this;

        $primaryValue = $this->session->get('user');
        if ( $primaryValue ) {
            $primaryKey = $this->userClass::primaryKey();
            $this->user = $this->userClass::findOne([$primaryKey => $primaryValue]);
        }
    }

    public function setController(Controller $controller) {
        $this->controller = $controller;
        if ( $this->user ) {
            $this->controller->setLayout('auth');
        }        
    }

    public function getController(): Controller {
        return $this->controller;
    }

    /**
     * Runs the aplication
     *
     * @return void
     */
    public function run(): void
    {
        try {
            echo $this->router->resolve();
        } catch (\Exception $e) {
            echo $this->router->renderErrorView($e->getCode());
        }
    }

    public function login(DbModel $user) {
        $this->user = $user;

        $primaryKey = $user->primaryKey();

        $primaryValue = $user->{$primaryKey};

        $this->session->set('user', $primaryValue);

        return true;
    }

    public static function isGuest() {
        return !self::$app->user;
    }

    public function logout() {
        $this->user = null;
        $this->session->remove('user');
    }
}