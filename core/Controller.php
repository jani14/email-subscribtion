<?php

namespace app\core;

use app\core\middlewares\BaseMiddleware;

class Controller {

    protected string $layout = 'main';
    protected string $action = '';

    /**
     * @var \app\core\middlewares\BaseMiddleware[]
     */
    protected array $middlewares = [];

    public function setLayout(string $layout) {
        $this->layout = $layout;
    }

    public function getLayout() {
        return $this->layout;
    }

    public function setAction(string $action) {
        $this->action = $action;
    }

    public function getAction() {
        return $this->action;
    }    


    public function render($view) {
        return Application::$app->router->renderView($view);
    }


    public function registerMiddleware(BaseMiddleware $middleware)
    {
        $this->middlewares[] = $middleware;
    }

    /**
     * @return BaseMiddleware[]
     */
    public function getMiddlewares(): array
    {
        return $this->middlewares;
    }    


    public function assign(string $name, $value) {
        Application::$app->router->assign($name, $value);
    }
}