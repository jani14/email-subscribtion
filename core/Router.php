<?php

namespace app\core;
/**
 * Router core class
 * 
 * @author Jani <jani.paakkola@hotmail.com>
 * @package app\core
 */
class Router {

    /**
     * Array containing get and post routes
     *
     * @var array
     */
    protected array $routes = [];


    protected array $assigns = [];

    /**
     * Request class for router to use
     *
     * @var Request
     */
    public Request $request;

    /**
     * Response class
     *
     * @var Response
     */
    public Response $response;

    /**
     * __construct
     *
     * @param Request $request
     */
    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }


    public function assign(string $name, $value) 
    {
        $this->assigns[$name] = $value;
    }

    /**
     * Get
     *
     * @param string $path
     * @param mixed $callback callback function or view
     * @return void
     */
    public function get(string $path, $callback): void 
    {
        $this->routes['get'][$path] = $callback;
    }


    public function post(string $path, $callback) 
    {
        $this->routes['post'][$path] = $callback;
    }

    /**
     * Resolves route
     *
     * @return string
     */
    public function resolve(): string
    {
        $path   = $this->request->getPath();
        $method = $this->request->getMethod();

        $callback = $this->routes[$method][$path] ?? false;
        
        if ( $callback === false ) {
            $this->response->setStatusCode(404);
            return $this->renderErrorView(404);
        }


        if ( is_string($callback) ) {
            return $this->renderView($callback);
        }

        if ( is_array($callback) ) {
            $controller = new $callback[0];
            $action = $callback[1] ?? '';
            $controller->setAction($action);
            Application::$app->setController($controller);

            $middlewares = $controller->getMiddlewares();
            foreach ($middlewares as $middleware) {
                $middleware->execute();
            }            

            $callback[0] = Application::$app->getController();
        }

        return call_user_func($callback);
    }


    /**
     * Renders the view
     *
     * @param string $view
     * @return string
     */
    public function renderView(string $view): string 
    {
        $layoutContent = $this->layoutContent();
        $viewContent = $this->renderOnlyView($view);

        // Join the view content in to layout
        return str_replace('{{content}}', $viewContent, $layoutContent);
    }

    public function renderErrorView(int $code) {
        ob_start(); // Cache the included layout
        include_once Application::$ROOT_DIR."/views/error_{$code}.php";
        return ob_get_clean(); // Clean cache and return the layout
    }

    /**
     * Get the base layout
     *
     * @return string
     */
    protected function layoutContent() :string
    {
        $layout = Application::$app->getController()->getLayout();
        ob_start(); // Cache the included layout
        include_once (Application::$ROOT_DIR."/views/layouts/{$layout}.php");
        return ob_get_clean(); // Clean cache and return the layout
    }


    /**
     * Render the view
     *
     * @param string $view
     * @return string
     */
    protected function renderOnlyView(string $view) :string
    {

        foreach ( $this->assigns as $key => $value ) {
            $$key = $value;
        }
        ob_start(); // Cache the included layout
        include_once (Application::$ROOT_DIR."/views/{$view}.php");
        $view = ob_get_clean(); // Clean cache and return the layout
        foreach ( $this->assigns as $key => $value ) {
            if ( is_string($value) ) {
                $view = str_replace("{{".$key."}}", $value, $view);
            }
        }
        
        return $view;
    }
}