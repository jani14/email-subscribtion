<?php

namespace app\core;

abstract class DbModel extends Model {

    abstract public static function tableName():string;

    abstract public function attributes():array;

    abstract public static function primaryKey():string;

    public function save() {
        try {
            $tableName = $this->tableName();
            $attributes = $this->attributes();
            $params = array_map(fn($attr) => ":$attr", $attributes);
            $stm = self::prepare("INSERT INTO {$tableName} (".implode(',',$attributes).") VALUES(".implode(',',$params).")");
            foreach ( $attributes as $attribute ) {
                $stm->bindParam(':'.$attribute, $this->{$attribute});
            }
            $stm->execute();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function findOne(array $where) {
        $attributes = array_keys($where);
        $params = implode(" AND ",array_map(fn($attr) => $attr.' = :'.$attr, $attributes));
        $stm  = self::prepare("SELECT * 
                                FROM " . static::tableName() . "
                                WHERE " . $params
                                );
        foreach ( $where as $key => $value ) {
            $stm->bindParam(":{$key}", $value);
        }

        $stm->execute();

        return $stm->fetchObject(static::class);
    }

    public static function findMany(array $where) {
        $objects = [];
        $table = static::tableName();
        $attributes = array_keys($where);
        $params = implode(" AND ",array_map(fn($attr) => $attr.' = :'.$attr, $attributes));
        $whereTerm = !empty($params) > 0 ? "WHERE {$params}" : "";
        $stm  = self::prepare("SELECT * FROM {$table} {$whereTerm}");
        if ( !empty($params) ) {
            foreach ( $where as $key => $value ) {
                $stm->bindParam(":{$key}", $value);
            }
        }

        $stm->execute();

        while ($object = $stm->fetchObject(static::class)) {
            $objects[] = $object;
        }

        return $objects;
    }    

    public static function prepare(string $sql) {
        return Application::$app->db->pdo->prepare($sql);
    }
}