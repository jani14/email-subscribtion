<?php

namespace app\core;

class Database {


    public \PDO $pdo;

    public function __construct(array $config)
    {
        $dsn = $config['dsn'] ?? '';
        $user = $config['user'] ?? '';
        $password = $config['password'] ?? '';
        $this->pdo = new \PDO($dsn, $user, $password);
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function applyMigrations() {
        $this->createMigrationsTable();
        $appliedMigrations = $this->getAppliedMigrations();

        $files = scandir(Application::$ROOT_DIR.'/migrations');
        $toApplyMigrations = array_diff($files, $appliedMigrations);

        $newMigrations = [];

        foreach ( $toApplyMigrations as $migration ) {
            if ( $migration === '.' || $migration === '..' ) {
                continue;
            }
            $this->log("Applying migration: " . $migration);
            require_once Application::$ROOT_DIR.'/migrations/'.$migration;
            $className = pathinfo($migration, PATHINFO_FILENAME);

            $instance = new $className();

            $instance->up();
            $this->log("Applyed migration: " . $migration);
            $newMigrations[] =  $migration;
        }

        if (! empty($newMigrations) ) {
            $this->saveMigrations($newMigrations);
        } else {
            $this->log("All migrations applied.");
        }
    }

    public function saveMigrations(array $migrations) {
        $migrationTmp = "";
        $stm = $this->pdo->prepare("INSERT INTO migrations (migration) VALUES (:migration)");
        $stm->bindParam(':migration', $migrationTmp, \PDO::PARAM_STR);

        foreach ( $migrations as $migration ) {
            $migrationTmp = $migration;
            $stm->execute();
        }
    }

    public function getAppliedMigrations() {
        $stm = $this->pdo->prepare("SELECT migration FROM migrations");
        $stm->execute();

        return $stm->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function createMigrationsTable() {
        $this->pdo->exec("CREATE TABLE IF NOT EXISTS migrations (
            id INT AUTO_INCREMENT PRIMARY KEY,
            migration VARCHAR(255), 
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        ) ENGINE=INNODB;");
    }

    protected function log(string $message) {
        echo $message . PHP_EOL;
    }
}