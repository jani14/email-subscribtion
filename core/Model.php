<?php

namespace app\core;

abstract class Model {

    public const RULE_REQUIRED      = 'required';
    public const RULE_EMAIL         = 'email';
    public const RULE_MIN           = 'min';
    public const RULE_MAX           = 'max';
    public const RULE_MATCH         = 'match';
    public const RULE_UNIQUE        = 'unique';

    public array $errors            = [];

    public abstract function rules(): array;
    public abstract function labels(): array;

    public function loadData(array $data) {
        foreach ( $data as $key => $value ) {
            if ( property_exists($this, $key) ) {
                $this->{$key} = $value;
            }
        }
    }

    public function validate() {
        foreach ( $this->rules() as $attribute => $rules ) {
            $value = $this->{$attribute};
            foreach ( $rules as $rule ) {
                $ruleName = $rule;
                if (! is_string($ruleName) ) {
                    $ruleName = $rule[0];
                }

                if ( $ruleName == self::RULE_REQUIRED && !$value ) {
                    $this->addRuleError($attribute, $ruleName);
                }

                if ( $ruleName == self::RULE_EMAIL && !filter_var($value, FILTER_VALIDATE_EMAIL) ) {
                    $this->addRuleError($attribute, $ruleName);
                }

                if ( $ruleName == self::RULE_MIN && strlen($value) < $rule['min']) {
                    $this->addRuleError($attribute, $ruleName, $rule);
                }

                if ( $ruleName == self::RULE_MAX && strlen($value) > $rule['max'] ) {
                    $this->addRuleError($attribute, $ruleName, $rule);
                }

                if ( $ruleName == self::RULE_MATCH && $this->{$rule['match']} !== $value ) {
                    $this->addRuleError($attribute, $ruleName, $rule);
                } 
                
                if ( $ruleName == self::RULE_UNIQUE ) {
                    $className = $rule['class'];
                    $uniqueAttribute = $rule['attribute'] ?? $attribute;
                    $tableName = $className::tableName();

                    $stm = Application::$app->db->pdo->prepare("SELECT * FROM {$tableName} WHERE $uniqueAttribute = :$uniqueAttribute LIMIT 1");
                    $stm->bindParam(':'.$uniqueAttribute, $value);
                    $stm->execute();

                    $record = $stm->fetchObject();

                    if ( $record ) {
                        $this->addRuleError($uniqueAttribute, self::RULE_UNIQUE, ['field' => $uniqueAttribute]);
                    }
                }
            }
        }

        return empty($this->errors);
    }

    private function addRuleError(string $attribute, string $rule, array $params = []) {
        $message = $this->errorMessages()[$rule] ?? '';
        foreach ( $params as $key => $value ) {
            $message = str_replace("{{$key}}", $value, $message);
        }

        $this->errors[$attribute][] = $message;
    }

    public function addError(string $attribute, string $message) {
        $this->errors[$attribute][] = $message;
    }    


    public function errorMessages() {
        return [
            self::RULE_REQUIRED => 'This field is required.',
            self::RULE_EMAIL => 'This field must be valid email address.',
            self::RULE_MIN => 'Min length of this field must be {min}',
            self::RULE_MAX => 'Max length of this field must be {max}',
            self::RULE_MATCH => 'This field must be the same as {match}',
            self::RULE_UNIQUE => 'Record with this {field} already exists',
        ];
    }

    public function hasErrors(string $attribute) {
        return $this->errors[$attribute] ?? false;
    }

    public function getFirstError(string $attribute) {
        return $this->errors[$attribute][0] ?? '';
    }
    
}