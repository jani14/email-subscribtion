<?php

namespace app\core;
/**
 * Request core class
 * All request goes from here
 * 
 * @author Jani <jani.paakkola@hotmail.com>
 * @package app\core
 */
class Request {


    protected static Request $instance;

    public function __construct()
    {
        self::$instance = $this;
    }


    public static function getInstance() {
        return self::$instance;
    }

    /**
     * Get request path
     *
     * @return string
     */
    public function getPath(): string
    {
        $path = $_SERVER['REQUEST_URI'] ?? '/';

        $position = strpos($path, '?');

        
        if ( $position === false ) {
            return $path;
        }

        return substr($path, 0, $position);
    }

    /**
     * Get method
     *
     * @return string
     */
    public function getMethod():string
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }


    public function getBody() {
        $body = [];

        if ( $this->getMethod() === "get" ) {
            foreach ( $_GET as $key => $value ) {
                $body[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }

        if ( $this->getMethod() === "post" ) {
            foreach ( $_POST as $key => $value ) {
                $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }
        
        return $body;
    }
}