<?php

namespace app\core;
/**
 * Response
 * 
 * @author Jani <jani.paakkola@hotmail.com>
 * @package app\core
 */
class Response {

    /**
     * Set status code
     *
     * @param integer $code
     * @return void
     */
    public function setStatusCode(int $code): void
    {
        http_response_code($code);
    }

    public function redirect(string $url) {
        header('Location: '.$url);exit();
    }
}