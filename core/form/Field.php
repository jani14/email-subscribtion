<?php

namespace app\core\form;

use app\core\Model;

class Field {

    public Model $model;

    public string $attribute;

    public string $type;

    public function __construct(Model $model, string $attribute, string $type)
    {
        $this->model = $model;
        $this->attribute = $attribute;
        $this->type = $type;
    }

    public function __toString()
    {
        return sprintf('
        <div class="form-group mt-3">
            <input placeholder="%s" type="%s" value="%s" class="form-control%s"  name="%s" id="%s" >
            <div class="invalid-feedback">%s</div>
        </div>        
        ', 
        $this->model->labels()[$this->attribute] ?? $this->attribute, 
        $this->type,
        $this->model->{$this->attribute},
        $this->model->hasErrors($this->attribute) ? ' is-invalid' : '',
        $this->attribute, 
        $this->attribute,
        $this->model->getFirstError($this->attribute)
        );
    }
}