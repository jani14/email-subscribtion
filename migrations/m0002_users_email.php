<?php

class m0002_users_email {
    public function up() {
        $db = \app\core\Application::$app->db;
        $sql = "ALTER TABLE `users` ADD COLUMN `email` VARCHAR(255) NOT NULL";

        $db->pdo->exec($sql);
    }

    public function down() {
        $db = \app\core\Application::$app->db;
        $sql = "ALTER TABLE `users` DROP COLUMN `email`";

        $db->pdo->exec($sql);      
    }
}

