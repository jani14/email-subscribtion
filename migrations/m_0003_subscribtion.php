<?php

class m_0003_subscribtion {
    public function up() {
        $db = \app\core\Application::$app->db;
        $sql = "ALTER TABLE `subscriptions`
        CHANGE COLUMN `subscripe_date` `subscribe_date` DATETIME NOT NULL COMMENT 'Date subscriped',
        CHANGE COLUMN `unsubscripe_date` `unsubscribe_date` DATETIME NULL DEFAULT NULL COMMENT 'Date unsubscriped'";

        $db->pdo->exec($sql);

        $sql = "RENAME TABLE `subscriptions` TO `subscribtions`";
        $db->pdo->exec($sql);
    }

    public function down() {
        $db = \app\core\Application::$app->db;
        $sql = "ALTER TABLE `subscriptions`
        CHANGE COLUMN `subscribe_date` `subscripe_date` DATETIME NOT NULL COMMENT 'Date subscriped',
        CHANGE COLUMN `unsubscribe_date` `unsubscripe_date` DATETIME NULL DEFAULT NULL COMMENT 'Date unsubscriped'";

        $db->pdo->exec($sql);

        $sql = "RENAME TABLE `subscribtions` TO `subscriptions`";
        $db->pdo->exec($sql);     
    }
}

