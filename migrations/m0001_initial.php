<?php

class m0001_initial {
    public function up() {
        $db = \app\core\Application::$app->db;
        $sql = "CREATE TABLE users (
            usersid INT AUTO_INCREMENT PRIMARY KEY,
            firstname VARCHAR(255) NOT NULL, 
            lastname VARCHAR(255) NOT NULL, 
            password VARCHAR(512) NOT NULL,
            status TINYINT NOT NULL, 
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        ) ENGINE=InnoDB;";

        $db->pdo->exec($sql);

        $sql = "CREATE TABLE subscriptions (
            `idsubscriptions` MEDIUMINT(8) AUTO_INCREMENT ,
            `email` VARCHAR(256) NOT NULL DEFAULT '' COMMENT 'email address' ,
            `active` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0 = Not active subscription, 1 = Active subscription',
            `subscripe_date` DATETIME NOT NULL COMMENT 'Date subscriped',
            `unsubscripe_date` DATETIME NULL DEFAULT NULL COMMENT 'Date unsubscriped',
            PRIMARY KEY (`idsubscriptions`) USING BTREE,
            UNIQUE INDEX `Email` (`email`) USING BTREE
        )
        COMMENT='Table to hold information about email addresses who has subscription'
        ENGINE=InnoDB
        ;";

        $db->pdo->exec($sql);
    }

    public function down() {
        $db = \app\core\Application::$app->db;
        $sql = "DROP TABLE users";

        $db->pdo->exec($sql);

        $sql = "DROP TABLE subscriptions";

        $db->pdo->exec($sql);        
    }
}