<?php

namespace app\models;

use app\core\DbModel;

class SubscribeModel extends DbModel {
    public static $tableName = 'subscribtions';
    public string $email = '';
    public int $active = 1;
    public string $subscribe_date = '';
    public ?string $unsubscribe_date = null;

    public static function tableName():string {
        return self::$tableName;
    }

    public static function primaryKey():string {
        return 'usersid';
    }

    public function save() {
        $this->active = 1;
        $this->subscribe_date = date("Y-m-d H:i:s");
        if ( parent::save() ) {
            return true;
        }
        return false;
    }

    public function rules():array {
        return [
            'email' => [self::RULE_REQUIRED, self::RULE_EMAIL, [self::RULE_UNIQUE, 'class' => self::class, 'attribute' => 'email']]
        ];
    }

    public function labels():array {
        return [
            'email' => 'Email'
        ];
    }


    public function attributes(): array {
        return [
            'email',
            'active',
            'subscribe_date',
            'unsubscribe_date'
        ];
    }
}