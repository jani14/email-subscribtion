<?php

namespace app\models;

use app\core\DbModel;

class UserModel extends DbModel {

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 2;

    protected static string $tableName = "users";
    
    public string $lastname = "";

    public string $firstname = "";

    public string $password = "";

    public string $confirmPassword = "";

    public string $email = "";

    public int $status = self::STATUS_INACTIVE;

    public static function tableName():string {
        return self::$tableName;
    }

    public static function primaryKey():string {
        return 'usersid';
    }

    public function save() {
        $this->status = self::STATUS_INACTIVE;
        $password = $this->password;
        $this->password = password_hash($this->password, PASSWORD_DEFAULT);
        if ( parent::save() ) {
            return true;
        }
        $this->password = $password;
        return false;
    }

    public function rules():array {
        return [
            'lastname' => [self::RULE_REQUIRED], 
            'firstname' => [self::RULE_REQUIRED],
            'email' => [self::RULE_REQUIRED, self::RULE_EMAIL, [self::RULE_UNIQUE, 'class' => self::class, 'attribute' => 'email']],
            'password' => [self::RULE_REQUIRED, [self::RULE_MIN, 'min' => 8], [self::RULE_MAX, 'max' => 24]],
            'confirmPassword' => [self::RULE_REQUIRED, [self::RULE_MATCH, 'match' => 'password']]
        ];
    }

    public function labels():array {
        return [
            'lastname' => 'Last name',
            'firstname' => 'First name',
            'email' => 'Your email',
            'password' => 'Your password',
            'confirmPassword' => 'Confirm your password',
        ];
    }


    public function attributes(): array {
        return [
            'lastname',
            'firstname',
            'email',
            'password',
            'status'
        ];
    }


}