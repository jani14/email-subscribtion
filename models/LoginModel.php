<?php

namespace app\models;

use app\core\Application;
use app\core\Model;

class LoginModel extends Model {

    public string $email = '';
    public string $password = '';

    public function rules():array {
        return [
            'email' => [self::RULE_REQUIRED, self::RULE_EMAIL],
            'password' => [self::RULE_REQUIRED, [self::RULE_MIN, 'min' => 8], [self::RULE_MAX, 'max' => 24]]
        ];
    }

    public function labels():array {
        return [
            'email' => 'Your email',
            'password' => 'Your password'
        ];
    }

    public function login() {
        $user = UserModel::findOne(['email' => $this->email]);

        if (! $user ) {
            $this->addError('email', 'Login failed. Verify that you have set correct login information.');
            return false;
        }

        if (! password_verify($this->password, $user->password) ) {
            $this->addError('password', 'Login failed. Verify that you have set correct login information.');
            return false;
        }

        return Application::$app->login($user);
    }
}