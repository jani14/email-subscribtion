<div class="row justify-content-center">
    <div class="col-xl-6" >
        <div class="text-center text-white">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                <img class="img-fluid rounded-circle mb-3" style="height:150px;" src="assets/img/me2.png" alt="...">
                <h5 class="">Jani Paakkola</h5>
                <p class="font-weight-light mb-0">"Thanks to this subcscribtion i now have a good mustache!"</p>
            </div>
            <?php if (!$subscribed): ?>           
            <h1 class="mb-5">Subscribe now!</h1>
            <?php endif; ?>
            <?php if ($subscribed): ?>           
            <h1 class="mb-5">Thank you for subscribing!</h1>
            <?php endif; ?>            
            <form class="form-subscribe" method="post" action="/subscribe">
                <div class="row <?php if ($subscribed): ?> d-none <?php endif; ?>">
                    <div class="col">
                        <input class="form-control form-control-lg" id="emailAddress" name="email" type="email" placeholder="Email Address" data-sb-validations="required,email" />
                        <div class="invalid-feedback text-white" data-sb-feedback="emailAddress:required">Email Address is required.</div>
                        <div class="invalid-feedback text-white" data-sb-feedback="emailAddress:email">Email Address Email is not valid.</div>
                    </div>
                    <div class="col-auto"><button class="btn btn-primary btn-lg" type="submit">Submit</button></div>
                </div>
                <div class="<?php if (!$subscribed): ?> d-none <?php endif; ?>" id="submitSuccessMessage">
                    <div class="text-center mb-3">
                        <div class="fw-bolder">You will get an email later.</div>
                    </div>
                </div>
                <div class="<?php if (!app\core\Application::$app->session->getFlash('subscribe_error')): ?> d-none <?php endif; ?> " id="submitErrorMessage"><div class="text-center text-danger mb-3"><?php echo app\core\Application::$app->session->getFlash('subscribe_error') ?></div></div>
            </form>
        </div>
    </div>
</div>