<div class="row justify-content-center">
    <div class="col-xl-6" >
        <div class="text-center text-white">          
            <h1 class="mb-5">Subscribtions</h1>
            <?php if (!count($subscribtions) > 0): ?>
                <h4>Sorry! No one has subsrcibed yet!</h4>
            <?php endif; ?>
        </div>
    </div>
</div>           
<?php if (count($subscribtions) > 0): ?>
<table class="table table-dark table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Email</th>
      <th scope="col">Subscribed at</th>
      <th scope="col">Active</th>
      <th scope="col">Unsubscribed at</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($subscribtions as $subscribtion): ?>
    <tr>
      <th scope="row"><?php echo $subscribtion->idsubscriptions ?></th>
      <td><?php echo $subscribtion->email ?></td>
      <td><?php echo $subscribtion->subscribe_date ?></td>
      <td><?php  if ($subscribtion->active) {echo "Yes";} else { echo "No"; } ?></td>
      <td><?php if ($subscribtion->unsubscribe_date) {echo $subscribtion->unsubscribe_date;} else {echo "-";} ?></td>
    </tr>
    <?php endforeach;?>
  </tbody>
</table>
<?php endif; ?>