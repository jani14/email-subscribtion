<h1 class="mt-3">Create an account!</h1>
<?php $form =  app\core\form\Form::begin('','post') ?>
    <div class="row">
        <div class="col">
            <?php echo $form->field($model, 'firstname') ?>
        </div>
        <div class="col">
            <?php echo $form->field($model, 'lastname') ?>
        </div>
    </div>
    <?php echo $form->field($model, 'email') ?>
    <?php echo $form->field($model, 'password', 'password') ?>
    <?php echo $form->field($model, 'confirmPassword', 'password') ?>
<div class="form-group mt-3">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>    
<?php echo app\core\form\Form::end() ?>   
<form>