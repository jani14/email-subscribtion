<h1 class="mt-3">Login</h1>
<?php $form =  app\core\form\Form::begin('','post') ?>
    <?php echo $form->field($model, 'email') ?>
    <?php echo $form->field($model, 'password', 'password') ?>
<div class="form-group mt-3">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>    
<?php echo app\core\form\Form::end() ?>   